#!/bin/bash

#melakukan percobaan koneksi pada google.com
wget -q --tries=10 --timeout=20 --spider http://google.com
#menampilkan  hasil dari test koneksi
if [[ $? -eq 0 ]]; then
	echo "Online"
else
	echo "Offline"
fi

#Melakukan script otomasi dengan crontab yang akan mengatur pemanggilan script 10 menit sekali
# */10 * * * * /root/shell_script/bin/online_checker.sh
# */10 * * * * /root/shell_script/bin/online_checker.sh > /dev/tty1
