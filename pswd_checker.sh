echo "Please enter your password:"

#membaca user password
read user_password
#memeriksa validasi kekuatan password dengan spesifikasi
# 	Minimal 8 Karakter
#	Minimal terdapat huruf kecil, huruf besar, bilangan, dan simbol
if [[ ${#user_password} -ge 8 && "user_password" != `whoami` && "$user_password" =~ [A-Z] && "$user_password" =~ [a-z] && "$user_password" =~ [0-9] && "$user_password" =~ [^A-Za-z0-9] && ! `echo $user_password | grep "$USER"` ]];
then
	echo "Good Password"
else
	echo "Weak Password"
fi

#menampilkan password yang di input
echo "password yang kamu masukan: " $user_password
